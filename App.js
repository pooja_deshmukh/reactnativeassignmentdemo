/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import React, {Component} from 'react';
import 'react-native-gesture-handler';
// import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { SplashScreen }  from './Screens/splashscreen';
import  WelcomeScreen  from './Screens/welcomescreen';
import  DashBoardScreen  from './Screens/dashboardscreen';
import { MapViewClass } from './Screens/Mapview';
import { SafeAreaView, StyleSheet, ScrollView, View,Text,StatusBar,} from 'react-native';
import { Header,LearnMoreLinks, Colors,DebugInstructions,ReloadInstructions, } from 'react-native/Libraries/NewAppScreen';

const Stack = createStackNavigator();

export default class App extends Component{
  render(){
    return(
          // <SplashScreen />
          <NavigationContainer>
            <Stack.Navigator initialRouteName="SplashScreen" >
              <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{headerShown: false}}
              />
              <Stack.Screen
                name="WelcomeScreen"
                component={WelcomeScreen}
                options={{headerShown: false}}
              />
              <Stack.Screen
                name="DashBoardScreen"
                component={DashBoardScreen}
                options={{headerShown: false}}
              />
              <Stack.Screen
                name="MapViewClass"
                component={MapViewClass}
                options={{headerShown: true}}
              />
            </Stack.Navigator>
          </NavigationContainer>
    );
  }
}
