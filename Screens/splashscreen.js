import React, { Component, useEffect } from 'react';  
import { View, SafeAreaView, Image,StyleSheet, Text } from 'react-native';


export class SplashScreen extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        setTimeout(() => { 
            this.props.navigation.navigate('WelcomeScreen') }, 2000)
    }
   render(){
       return (
               <View style={style.viewStyle}>
                  <Image style={style.imageStyles} source={require('../Screens/Resources/Splash.png')} />
               </View>
       );
   }
    
}

const style = StyleSheet.create({
    imageStyles :{
        height: 200,
        width: 240
    },
    viewStyle:{
        flex:1, 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#9FA4C4'
    } 
});


