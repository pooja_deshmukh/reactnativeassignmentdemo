import React, {Component} from 'react';
import {View, SafeAreaView, Text, StyleSheet,TouchableOpacity, ImageBackground,Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const DashBoardScreen = ({ navigation }) => {
    // render(){
        const OnContactUsTap = () => {
            navigation.navigate('MapViewClass');   
        };
        return(
            // <SafeAreaView style={style.bgView}>
            <View style={style.mainView}>
                 <LinearGradient colors={['#9FA4C4','#B3CDD1']} style={style.gradientbgView} >
            {/* <ImageBackground source={require('../Assignment1/Resources/welcomeBgImg.jpg')} style={style.image}> */}
                <View style={{flex: 1, margin: 20, paddingTop: 20, borderRadius: 10}}>
                <Text style={style.textStyle}>Dashboard</Text>
                <View style={style.mainView1}>
                <View style={style.innerView}>
                    <TouchableOpacity style={style.buttonStyle} activeOpacity={0.8}>
                    <Image style={{ width:100, height:100, marginBottom: 10}} source={require('../Screens/Resources/MyProfilebg.png')} />
                    <Text style={{ color: 'black'}}>Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.button2Style} activeOpacity={0.8}>
                    <Image style={{ width:100, height:100,marginBottom: 10}} source={require('../Screens/Resources/MyAccountbg.png')} />
                    <Text style={{ color: 'black'}}>My Account</Text>
                    </TouchableOpacity>
                </View>
                <View style={style.innerView}>
                    <TouchableOpacity style={style.buttonStyle} activeOpacity={0.8}>
                     <Image style={{ width:100, height:100, marginBottom: 10}} source={require('../Screens/Resources/Orders.png')} />
                     <Text style={{ color: 'black'}}>My Orders</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.button2Style} activeOpacity={0.8}>
                    <Image style={{ width:100, height:100,marginBottom: 10}} source={require('../Screens/Resources/MyOrdersbg.png')} />
                    <Text style={{ color: 'black'}}>My Cart</Text>
                    </TouchableOpacity>
                </View>
                </View>
                <View style={style.bottomView}>
                    <TouchableOpacity style={style.bottomButtonStyle} activeOpacity={0.8}>
                    <Text style={{ color:'white'}}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.bottomButtonStyle} onPress={OnContactUsTap} activeOpacity={0.8}>
                    <Text style={{ color:'white'}}>Contact Us</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.bottomButtonStyle} activeOpacity={0.8}>
                    <Text style={{ color:'white'}}>Settings</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {/* </ImageBackground> */}
            </LinearGradient>
            </View>
            // </SafeAreaView>
        );
    // }
}

const style = StyleSheet.create({
    gradientbgView:{
        flex: 1,
     },
    bgView:{
        flex: 1,
        backgroundColor: 'lightgray',
        fontSize: 30,
     },
     mainView:{
        flex:1,
        fontSize: 30
     },
     mainView1:{
        flex:1,
        margin: 20,
        fontSize: 30,
        justifyContent: 'center'
     },
    textStyle:{
        marginTop: 20,
        marginBottom: 20,
        fontSize: 25,
        alignSelf: 'center',
        color: 'black'
     },
    innerView: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    bottomView: {
        flexDirection: 'row',
        marginBottom: 10,
        backgroundColor: '#01579b'
    },
    buttonStyle:{
        flex:1,
        borderRadius: 4,
        borderColor: 'grey',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    button2Style:{
        flex:1,
        borderRadius: 4,
        borderColor: 'grey',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        marginLeft: 20
    },
    bottomButtonStyle:{
        flex:1,
        padding: 15,
        borderColor: 'grey',
        borderWidth: 1,
        alignItems: 'center',
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
      }
});

export default DashBoardScreen;