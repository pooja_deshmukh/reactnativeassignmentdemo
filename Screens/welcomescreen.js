import React, {Component, useState} from 'react'; 
import LinearGradient from 'react-native-linear-gradient'
import { View, Text, StyleSheet, SafeAreaView, TextInput, Button, TouchableOpacity,ImageBackground, Alert } from 'react-native';

// export class WelcomeScreen extends Component{
const WelcomeScreen = ({ navigation }) => {
    const [textInputName, setTextInputUserName] = useState('');
    const [textInputEmail, setTextInputPassword] = useState('');
    
    const checkTextInput = () => {
        //Check for the Name TextInput
        if (!textInputName.trim()) {
          alert('Please enter user Name');
          return;
        }
        //Check for the Email TextInput
        if (!textInputEmail.trim()) {
          alert('Please enter password');
          return;
        }
        navigation.navigate('DashBoardScreen'); 
    };
    const OnForgotPasswordTap = () => {
        alert('Functionality in progress');
    };
    const OnSignUpTap = () => {
        alert('Functionality in progress');
    };
    // render(){
        return (
            <SafeAreaView style={style.bgView}>
            <View style={style.bgView}>
            <LinearGradient colors={['#9FA4C4','#B3CDD1']} style={style.gradientbgView} >
                {/* <ImageBackground source={require('./welcomeBgImg.jpg')} style={style.image}> */}
                <View style={{justifyContent: 'center',borderColor:'grey', borderWidth: 0.6, margin: 20, paddingTop: 20, borderRadius: 10}}>
                <Text style={style.textStyle}>Welcome Screen</Text>
                <View style={style.mainViewStyle}> 
                 <View style={style.innerView}>
                  <TextInput style={style.textinputstyle} onChangeText={(value) => setTextInputUserName(value)} placeholder= "UserName"/>
                  </View>
                  <View style={style.innerView}>
                  <TextInput secureTextEntry={true} style={style.textinputstyle} onChangeText={(value) => setTextInputPassword(value)} placeholder= "Password"/>
                  </View>
                </View>
                {/* <Button style={style.textStyle} title='Forgot Password'/> */}
                <TouchableOpacity style={style.forgotButtonStyle} onPress={OnForgotPasswordTap} activeOpacity={0.8}>
                   <Text style={{ color:'black'}}>Forgot Password?</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.buttonStyle} onPress={checkTextInput} activeOpacity={0.8}>
                   <Text style={{ color:'white'}}>LOGIN</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.signbuttonStyle} onPress={OnSignUpTap} activeOpacity={0.8}>
                   <Text style={{ color:'white'}}>SIGNUP</Text>
                </TouchableOpacity>
                </View>
                {/* <Button style={style.buttonStyle} color='black' title='SIGNUP'/> */}
                {/* </ImageBackground> */}
                </LinearGradient>
            </View>
            </SafeAreaView>
        );
    // }
}

const style = StyleSheet.create({
    gradientbgView:{
        flex: 1,
        justifyContent: 'center'
     },
    bgView:{
       flex: 1,
       backgroundColor: 'black',
       fontSize: 30,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
      },
    textStyle:{
        marginTop: 20,
       marginBottom: 20,
       fontSize: 25,
       color: 'black',
       alignSelf: 'center',
    },
    labelStyle:{
        fontSize: 20, 
        flex: 1  
     },
    textinputstyle:{
        fontSize: 18,
        alignSelf: 'center',
        borderRadius: 6,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'transparent',
        paddingVertical: 10,
        marginLeft: 10,
        marginRight: 10,
        flex: 2,
        paddingLeft: 10
     },
     mainViewStyle: {
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 20,
        backgroundColor: 'transparent'
     }, 
    innerView: {
        //flex: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20
    },
    buttonStyle:{
        elevation: 8,
        borderRadius: 10,
        borderColor: 'grey',
        backgroundColor:'#01579b',
        borderWidth: 1,
        paddingVertical: 16,
        margin: 25,
        alignItems: 'center',        
    },
    signbuttonStyle:{
        elevation: 8,
        borderRadius: 10,
        borderColor: 'grey',
        backgroundColor:'#01579b',
        borderWidth: 1,
        paddingVertical: 16,
        marginRight: 25,
        marginLeft: 25,
        alignItems: 'center',  
        marginBottom: 20
    },
    forgotButtonStyle:{
        marginTop:0,
        marginEnd: 20,
        alignSelf: 'flex-end',
    }
});

export default WelcomeScreen;