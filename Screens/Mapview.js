import React, { Component } from 'react';  
import { StyleSheet, View } from 'react-native';  
import MapView from 'react-native-maps';  
import { Marker } from 'react-native-maps';  
navigator.geolocation = require('@react-native-community/geolocation');
  
export class MapViewClass extends Component {
  latitude;
  longitude
  constructor(){  
    super();  
    this.state = {  
        ready: false,  
        // where: {lat:null, lng:null},  
        error: null  
    }  
  }  
  componentDidMount(){  
    let geoOptions = {  
        enableHighAccuracy:false,  
        timeOut: 20000, //20 second  
      //  maximumAge: 1000 //1 second  
    };  
    this.setState({ready:false, error: null });  
    navigator.geolocation.getCurrentPosition( this.geoSuccess,  
        this.geoFailure,  
        geoOptions);  
}  
geoSuccess = (position) => {  
    console.log(position.coords.latitude);  
    // this.latitude = position.coords.latitude;
    // this.longitude = position.coords.longitude;
    this.setState({  
        ready:true,  
        where: {lat: position.coords.latitude,lng:position.coords.longitude }  
    })  
}  
    geoFailure = (err) => {  
        this.setState({error: err.message});  
    }    

  render() {  
    return (  
      <View style={styles.MainContainer}>  
  
        <MapView
          style={styles.mapStyle}  
          showsUserLocation={true}  
          zoomEnabled={true}  
          zoomControlEnabled={true}  
          // initialRegion={{  
          //   latitude: 28.636645,   
          //   longitude: 77.096364,  
          //   latitudeDelta: 0.0922,  
          //   longitudeDelta: 0.0421,  
          // }}
          // initialRegion={{
          //   latitude: 28.636645,
          //   longitude: 77.096364,
          //   latitudeDelta: 0.0922,
          //   longitudeDelta: 0.0421,
          // }}
           >  
   
          <Marker
            initialRegion={{ latitude: this.latitude, longitude: this.longitude }}
            coordinate={{ latitude: this.latitude, longitude: this.longitude }}  
            accessibilityLiveRegion={{ latitude: this.latitude, longitude: this.longitude }}
            title={"Pooja Deshmukh"}  
            description={"Pooja Deshmukh Home"}  
          />  
        </MapView>  
          
      </View>  
    );  
  }  
}  
  
const styles = StyleSheet.create({  
  MainContainer: {  
    position: 'absolute',  
    top: 0,  
    left: 0,  
    right: 0,  
    bottom: 0,  
    alignItems: 'center',  
    justifyContent: 'flex-end',  
  },  
  mapStyle: {  
    position: 'absolute',  
    top: 0,  
    left: 0,  
    right: 0,  
    bottom: 0,  
  },  
});  